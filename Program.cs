﻿using System;

namespace week4_ex21
{
    class Program
    {
        static void Main(string[] args)
        {
/*
            var counter = 5;

            for (var i = 0; i < counter; i++)
            {
                var a = i + 1;
                Console.WriteLine($"This is line number {a} (For loop)");
            }
*/     
/*            //Similar outcome to a for loop
            //Will loop until condition is false
            var counter = 5;
            var i = 0;

            while (i < counter)
            {
                var a = i + 1;
                Console.WriteLine($"This is line number {a} (While loop)");

                i++;
            }*/

            int i = 0;

            for (i = 0; i < 20; i++)
            {
                Console.WriteLine(i);
            }
            while(i % 2== 1)
            {
                Console.WriteLine(i);
            }
 
        }
    }
}
